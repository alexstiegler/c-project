﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace lifesimulation
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            Random dice = new Random();

            drawplace.Measure(new Size(double.PositiveInfinity, double.PositiveInfinity));
            drawplace.Arrange(new Rect(0.0, 0.0, drawplace.DesiredSize.Width, drawplace.DesiredSize.Height));

            for (int i = 0; i < numbersH; i++)
            {
                for (int j = 0; j < wide; j++)
                {
                    Rectangle r = new Rectangle();
                    r.Width = drawplace.ActualWidth / wide - 2.0;
                    r.Height = drawplace.ActualHeight / numbersH - 2.0;
                    r.Fill = (dice.Next(0, 2) == 1) ? Brushes.Cyan : Brushes.Red;
                    drawplace.Children.Add(r);
                    Canvas.SetLeft(r, j * drawplace.ActualWidth / wide);
                    Canvas.SetTop(r, i * drawplace.ActualHeight / numbersH);
                    r.MouseDown += R_MouseDown;

                    field[i, j] = r;
                }
            }

            timer.Interval = TimeSpan.FromSeconds(0.1);
            timer.Tick += Timer_Tick;
        }

        const int wide = 30;
        const int numbersH = 30;
        Rectangle[,] field = new Rectangle[wide, numbersH];
        DispatcherTimer timer = new DispatcherTimer();

        private void R_MouseDown(object sender, MouseButtonEventArgs e)
        {
            ((Rectangle)sender).Fill =
                (((Rectangle)sender).Fill == Brushes.Cyan) ? Brushes.Red : Brushes.Cyan;
        }

        private void Timer_Tick(object sender, EventArgs e)
        {
            int[,] neighbourcount = new int[numbersH, wide];
            for (int i = 0; i < numbersH; i++)
            {
                for (int j = 0; j < wide; j++)
                {
                    int iabove = i - 1;
                    if (iabove < 0)
                    { iabove = numbersH - 1; }
                    int ibelow = i + 1;
                    if (ibelow >= numbersH)
                    { ibelow = 0; }
                    int jleft = j - 1;
                    if (jleft < 0)
                    { jleft = wide - 1; }
                    int jright = j + 1;
                    if (jright >= wide)
                    { jright = 0; }

                    int neighbour = 0;

                    if (field[iabove, jleft].Fill == Brushes.Red)
                    { neighbour++; }
                    if (field[iabove, j].Fill == Brushes.Red)
                    { neighbour++; }
                    if (field[iabove, jright].Fill == Brushes.Red)
                    { neighbour++; }
                    if (field[i, jleft].Fill == Brushes.Red)
                    { neighbour++; }
                    if (field[i, jright].Fill == Brushes.Red)
                    { neighbour++; }
                    if (field[ibelow, jleft].Fill == Brushes.Red)
                    { neighbour++; }
                    if (field[ibelow, j].Fill == Brushes.Red)
                    { neighbour++; }
                    if (field[ibelow, jright].Fill == Brushes.Red)
                    { neighbour++; }

                    neighbourcount[i, j] = neighbour;
                }
            }

            for (int i = 0; i < numbersH; i++)
            {
                for (int j = 0; j < wide; j++)
                {
                    if(neighbourcount[i, j] < 2 || neighbourcount[i, j] > 3)
                    {
                        field[i, j].Fill = Brushes.Cyan;
                    }
                    else if (neighbourcount[i, j] == 3)
                    {
                        field[i, j].Fill = Brushes.Red;
                    }
                }
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if(timer.IsEnabled)
            {
                timer.Stop();
                buttonStartStop.Content = "Starting Animation!";
            }
            else
            {
                timer.Start();
                buttonStartStop.Content = "Stopping Animation!";
            }
        }
    }
}
